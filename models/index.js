const nconf = require('nconf')
const mongoose = require('mongoose')
const Promise = require('bluebird')

Promise.promisifyAll(mongoose)

!(function () {
    if (mongoose.connection.readyState !== 0) return

    const connectString = nconf.get('mongodb')
    return mongoose.connectAsync(connectString)
        .then(function () {
            console.log('Mongoose connected to: ' + connectString)
        })
        .catch(function (err) {
            console.log('ERROR connecting to: ' + connectString + '. ' + err)
        })
}())

module.exports = {
    ImageFile: require('./image').ImageFile
}