/**
 * Created by liang on 2017/8/1.
 */

const mongoose = require('mongoose')

const Schema = mongoose.Schema

//图片===================================================
const ImageFileSchema = new Schema({
    status          : {type: Number, required: true},   //图片状态
    domain_name     : {type: String, required: true},   //图片domain
    image_mime      : {type: String, required: true},   //图片mime
    image_path      : {type: String, required: true},   //图片存储路径
    image_url       : {type: String, required: true},   //图片地址
    image_name      : {type: String, required: true},   //图片名称
    upload_name     : {type: String, required: true},   //图片原名称
    create_time     : {type: Date,   required: true},   //创建时间
    update_time     : {type: Date,   required: true},   //更新时间
})

ImageFileSchema.virtual('id', function () {
    return this._id.toString()
})

ImageFileSchema.index({image_name : 1, unique: true})
ImageFileSchema.index({domain_name : 1})


//文件状态
ImageFileSchema.statics.STATUS = {
    ENABLE : 1,
    DISABLE : 0,
}

exports.ImageFile = mongoose.model('ImageFile', ImageFileSchema)
