const nconf = require('nconf')
const qn = require('qn')
const mongoose = require('mongoose')

const FileService = require('../lib/file').FileService
const fileService = new FileService(nconf.get('image'))
const QiNiuService = require('../lib/file').QiNiuService
const qiNiuService = new QiNiuService(nconf.get('qiniu'))

const ImageFile = require('../models').ImageFile

/**
 * @desc 保存图片
 * */
exports.saveUploadImage = function (domain, uploadName, ext, fileMime, readStream, callback) {
    function createImage(err, filePath, fileUrl) {
        if (err) {
            return callback(err)
        }

        // 写到数据库
        let imageDoc = {
            status: ImageFile.STATUS.ENABLE,    // 图片状态
            domain_name: domain,                // 图片domain
            image_mime: fileMime,               // 图片mime
            image_path: filePath,               // 图片存储路径
            image_url: fileUrl,                 // 图片地址
            image_name: fileName,               // 图片名称
            upload_name: uploadName,            // 图片原名称
            create_time: new Date(),            // 图片创建时间
            update_time: new Date()             // 图片更新时间
        }

        ImageFile.create(imageDoc, callback)
    }

    let fileName = mongoose.Types.ObjectId().toString() + ext

    // 写到磁盘
    //fileService.persistence(domain, fileName, readStream, createImage)

    // 写到七牛
    qiNiuService.persistence(domain, fileName, readStream, createImage)
}
