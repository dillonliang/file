FROM mhart/alpine-node:8.9.1

LABEL author=dillon

WORKDIR /www

RUN echo "Create WorkDir ..." \
	&& mkdir -p /www

COPY . .

RUN echo "Installing node moduloes ..." \
  && export npm_config_registry=http://10.10.152.203:4873/ \
  && export npm_config_loglevel=warn \
  && export NODEJS_ORG_MIRROR=https://npm.taobao.org/mirrors/node \
  && npm i yarn -g \
  && yarn install \
  && echo "Clean up ..." \
  && rm -rf /tmp ~/.npm /usr/local/share/.cache

STOPSIGNAL SIGTERM

VOLUME "/data"

EXPOSE 8080

ENTRYPOINT ["npm", "run", "dev"]