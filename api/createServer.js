const express = require('express')
const cors = require('cors')
const http = require('http')
const bodyParser = require('body-parser')
const responseTime = require('response-time')
const expressValidator = require('express-validator')

module.exports = function () {
	require('../lib/config')

    const app = express()

    app.use(cors())
	app.use(responseTime())
	app.use(bodyParser.json())
	app.use(bodyParser.urlencoded({
		extended: true
	}))
    app.use(expressValidator())
    
    require('./router')(app)

	const server = http.createServer(app)

	console.log('File Server ' + process.pid + ' Start.')

	return server
}