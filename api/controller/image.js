const formidable = require('formidable')
const mime = require('mime')
const path = require('path')

const imageManager = require('../../module/imageManager')

exports.route = function(app) {
    app.post('/upload/image', uploadImage)  // 上传图片
}

function uploadImage(req, res, next) {
    const form = new formidable.IncomingForm()

    form.keepExtensions = true

    let result = {
        ok : true,
        failed_message: null,
        success_message: null
    }

    form.onPart = function (part) {
        const self = this

        let fileName = part.filename

        if (!fileName) {
            return
        }

        let domain = !!part.domain ? part.domain.replace(path.sep, '') : 'other'
        let fileMime = part.mime
        let ext = path.extname(fileName).toLowerCase()

        if(!ext){
            ext = '.' + mime.extension(fileMime)
            fileName = fileName  + ext
        }

        if (ext !== '.jpg' && ext !== '.jpeg' && ext !== '.png') {
            return
        }

        self._flushing++

        //处理图片流
        imageManager.saveUploadImage(domain, fileName, ext, fileMime, part, function (err, image) {
            if (err) {
                logger.error(err)
            }

            if (image) {
                result.image = {
                    url: image.image_url,
                    upload_name: image.upload_name,
                    create_time: image.create_time
                }
            }

            self._flushing--
            self._maybeEnd()
        })

    }

    form.on('error' , function (err) {
        return next(err)
    })
    
    form.on('end', function () {
        res.json({
            flag: '0000',
            msg: '',
            result: result
        })
    })

    form.parse(req)
}