const server = require('./createServer')()
const port = process.env.PORT || 8080

/* eslint-disable no-console */
server.listen(port, function () {
	console.log(`File listening in ${port}`)
})