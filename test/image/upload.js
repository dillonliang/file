/**
 * Created by liang on 2017/10/16.
 */

const chai = require('chai');
const request = require('supertest');
const Mock = require('mockjs');

const app = require('../../app').app;

describe('图片上传', function () {

    it('#图片上传成功', function (done) {

        request(app)
            .post('/upload/image')
            .expect(200)
            .end(function (err, res) {
                if(err){
                    throw err;
                }

                chai.expect(res.body).to.have.property('flag', '0000');
                chai.expect(res.body).to.have.property('msg', '');
                chai.expect(res.body).to.have.ownProperty('result');
                done();
            });

    });
});