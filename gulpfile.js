/**
 * Created by liang on 2017/10/16.
 */

const gulp = require('gulp');
const mocha = require('gulp-mocha');

/**
 * 初始化环境
 * */
gulp.task('test_init_env', function (done) {

    if (!process.env.NODE_ENV) {
        process.env.NODE_ENV = 'dev';
    }

    if (process.env.NODE_ENV && process.env.NODE_ENV === 'pro') {
        return done(new Error('can not run test on this ' + process.env.NODE_ENV));
    }

    global.config = require('./config');
    done();
});

/**
 *  @desc 执行测试
 * */
gulp.task('test', ['test_init_env'], function (done) {
    console.log('test starting')

    let stream = gulp.src('./test/**/*.js')
        .pipe(mocha());

    stream.on('end', function () {
        done();
        process.exit(0);
    });

    stream.on('error', function (err) {
        if (err) {
            console.error(err.stack);
        }

        done();
        process.exit(0);
    });
});