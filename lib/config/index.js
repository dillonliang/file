const nconf = require('nconf')
const path = require('path')

nconf.use('memory')
nconf.env()

const env = process.NODE_ENV || 'development'
nconf.set('NODE_ENV', env)
const projectRoot = path.join(__dirname, '..', '..')

const CONFIG_DIR = path.resolve(projectRoot)
const config = require(`${CONFIG_DIR}/config/config.${env}.json`)

nconf.defaults(config)
