/**
 * Created by liang on 2017/8/1.
 */

const winston = require('winston');
const path = require('path');
const fs = require('fs');

class WinstonLogger {
    constructor (options) {
        if (!options) {
            throw new Error('config is needed')
        }

        this.__config = options;
        this.__type = options.type;
        this.__logger = null;

        this.logLevels = {
            levels: {
                debug: 0,
                trace: 1,
                info: 2,
                warn: 3,
                error: 4,
                fatal: 5
            },
            colors: {
                debug: 'black',
                trace: 'black',
                info: 'green',
                warn: 'yellow',
                error: 'red',
                fatal: 'red'
            }
        };

        this.init();

        return this.__logger;
    }

    init() {
        const self = this;

        winston.addColors(self.logLevels.colors);

        self.__logger = new (winston.Logger)({ levels: self.logLevels.levels });

        let types = self.__type.split('&');

        types = types.map(function (type) {
            return type.trim().toLowerCase();
        });

        types.forEach(function (type) {
            switch (type) {
                case 'console' : {
                    if (!self.__config.console) {
                        throw new Error('there is no console config in log');
                    }

                    self.__logger.add(winston.transports.Console, {
                        level: self.__config.console.level,
                        colorize: true
                    });
                }

                break;

                case 'file' : {
                    if (!self.__config.file) {
                        throw new Error('there is no console config in log');
                    }

                    if (!fs.existsSync(self.__config.file.path)) {
                        fs.mkdirSync(self.__config.file.path);
                    }

                    let filePath = path.join(self.__config.file.path, self.__config.file.filename);

                    self.__logger.add(winston.transports.File, {
                        level: self.__config.file.level,
                        path: self.__config.file.path,
                        filename: filePath,
                        maxsize: self.__config.file.maxsize
                    });
                }

                break;

                default : {
                    throw new Error('not support this type ' + type + 'support type list is : console file');
                }

                break;
            }
        });
    }
}

module.exports = WinstonLogger;