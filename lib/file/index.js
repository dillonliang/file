const path = require('path')
const fs = require('fs')
const url = require('url')
const qn = require('qn')

class FileService {
    /**
     * 实例应当包括：上传文件的跟路径，可以上传的domain，上传成功后可以访问的地址配置等
     * */
    constructor (options) {
        this.base = options.base
        this.domains = options.domains
        this.server = options.server
    }

    persistence (domain, fileName, readStream, callback) {
        const self = this

        if (!this.domains.includes(domain)) {
            return callback(new Error(domain + 'is not allow to upload'))
        }

        let filePath = path.join(this.base, domain, fileName)

        let writeStream = fs.createWriteStream(filePath)

        readStream.on('error', function (err) {
            callback(err)
            fs.unlink(filePath)
        })

        writeStream.on('error', function (err) {
            callback(err)
            fs.unlink(filePath)
        })

        writeStream.on('finish', function () {
            let fileUrl = url.format({
                protocol: self.server.protocol,
                hostname: self.server.hostname,
                // port: self.server.port,
                pathname: path.join(self.server.pathname, domain, fileName),
            })

            callback(null, filePath, fileUrl)
        })

        readStream.pipe(writeStream)
    }
}

class QiNiuService {
    constructor (options) {
        this.accessKey = options.accessKey
        this.secretKey = options.secretKey
        this.bucket = options.bucket
        this.origin = options.origin
    }

    persistence(domain, fileName, readStream, callback) {
        const client = qn.create({
            accessKey: this.accessKey,
            secretKey: this.secretKey,
            bucket: this.bucket,
            origin: this.origin
        })

        let filePath = path.join(domain, fileName)
        client.upload(readStream, { key: filePath}, function (err, result) {
            callback(err, filePath, result && result.url)
        })
    }
}

exports.FileService = FileService
exports.QiNiuService = QiNiuService
