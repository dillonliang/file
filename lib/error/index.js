/**
 * Created by liang on 2017/8/1.
 */

class HttpError extends Error {
    constructor(msg, code) {
        super(msg);

        this.code = code;
    }
}