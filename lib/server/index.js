/**
 * Created by liang on 2017/8/1.
 */
const cluster = require('cluster');
const event = require('events');
const express = require('express');
const os = require('os');

const EventEmitter = event.EventEmitter;

class ExpressServer extends EventEmitter {
    constructor (options) {
        super();

        options = options || {};

        this.__host = options.host || null;

        this.__key = options.key;

        this.__cert = options.cert;

        this.__port = {
            http: options.port.http,
            https: options.port.https
        };

        this.__defaultHeaders = {
            version : 'X-Accept-Version',
            channel : 'x-Client-Channel',
            cache   : 'X-Use-Api-Cache'
        };

        /**
         * 如果调用cluster方法启动程序，所有子进程都在这里
         * */
        this.workers = null;

        /**
         * express 创建的app，如果想调用express的相关方法，可以使用这个app
         * */
        this.app = null;

        this.init();
    }

    init() {
        this.app = express();
        return this.app;
    }

    // config
    config(fn) {
        fn(this.app);
    }

    // 中间件
    middleware(fn) {
        fn(this.app);
    }

    // 路由
    route(fn) {
        fn(this.app);
    }

    // err
    error(fn) {
        fn(this.app);
    }

    listen(callback) {
        const self = this;

        if (!self.__port.http && !self.__port.https) {
            throw new Error('port.http or port.https is not set');
        }

        let listenMessage = '';
        let flag = 0;
        let count = 0;

        if (self.__port.http) {
            count++;
        }

        if (self.__port.https) {
            count++;
        }

        if (self.__port.http) {

            self.app.listen(self.__port.http, self.__host, function (err) {

                flag++;

                if (err) {
                    listenMessage += err.message;
                }

                let host = self.__host || '127.0.0.1';

                listenMessage += 'server listen on : http://' +
                    host +
                    ':' +
                    self.__port.http +
                    ', pid : ' +
                    process.pid;

                if (flag >= count) {
                    self.emit('listen', self.__port.http);
                    callback && callback(listenMessage);
                }
            });

        }
    }

    cluster(number, callback) {
        const self = this;

        let cpuCount = number || os.cpus().length - 1;

        self.workers = {};

        if (cluster.isMaster) {

            for (let i = 0; i < cpuCount; i++) {

                let workerProcess = cluster.fork();

                self.workers[workerProcess.process.pid] = workerProcess;

                self.emit('childStart', workerProcess);

            }

            cluster.on('exit', function (worker, code, signal) {

                self.emit('childStop', worker, code, signal);

                delete self.workers[worker.process.pid];

                setTimeout(function(){

                    let workProcess = cluster.fork();

                    self.workers[workProcess.process.pid] = workProcess;

                    self.emit('childRestart', workProcess);

                }, 1000);

            });

        } else {
            self.listen(callback);
        }
    }
}

module.exports = ExpressServer;